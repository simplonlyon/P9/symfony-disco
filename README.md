# Symfony Disco
Un projet Symfony 4.2 à utiliser avec le projet js-disco pour voir les interaction client/serveur

## How To Use
1. `composer install`
2. Créer le .env.local avec les infos qu'il faut dedans et mettre à jour les infos du .env.test
3. Si on a pas les databases : `bin/console doctrine:database:create` et `bin/console doctrine:database:create --env=test`
4. `bin/console doctrine:migrations:migrate` puis `bin/console doctrine:fixtures:load`
5. Pour lancer les tests `bin/console doctrine:schema:update --env=test --force` puis `bin/phpunit`
