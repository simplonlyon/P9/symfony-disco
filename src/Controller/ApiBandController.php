<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BandRepository;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\BandType;
use App\Entity\Band;

/**
 * @Route("/api/band", name="api_band")
 */
class ApiBandController extends AbstractController
{

    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route(methods="GET")
     */
    public function index(BandRepository $repo)
    {
        $bands = $repo->findAll();
        return new JsonResponse($this->serializer->serialize($bands, 'json'), 200, [], true);
    }

    /**
     * @Route(methods="POST")
     */
    public function add(Request $request, ObjectManager $manager)
    {
        $band = new Band();
        $form = $this->createForm(BandType::class, $band);
        $form->submit(json_decode($request->getContent(), true));

        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($band);
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($band, 'json'), 201, [], true);
        }

        return new JsonResponse($form->getErrors(true), 400);
    }

    /**
     * @Route("/{id}",methods="DELETE")
     */
    public function delete(Band $band, ObjectManager $manager) {
        $manager->remove($band);
        $manager->flush();
        return new JsonResponse(null, 204);
    }

    /**
     * @Route("/{id}", methods="PATCH")
     */
    public function update(Band $band, ObjectManager $manager, Request $request) {
        
        $form = $this->createForm(BandType::class, $band);
        $form->submit(json_decode($request->getContent(), true), false);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $manager->flush();

            return new JsonResponse($this->serializer->serialize($band, 'json'), 200, [], true);
        }

        return new JsonResponse($form->getErrors(true), 400);
    }
}
