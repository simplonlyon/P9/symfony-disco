<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use App\Entity\Band;


class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($x=0; $x < 5; $x++) { 
            $band = new Band();
            $band->setName('Queen '.$x);
            $band->setStart(new \DateTime('1970-04-2'.$x));
            $band->setEnd(new \DateTime('2018-10-1'.$x));
            $band->setCountry('England');
            $manager->persist($band);
        }

        $manager->flush();
    }
}
