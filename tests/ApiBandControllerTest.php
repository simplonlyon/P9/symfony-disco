<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use App\DataFixtures\AppFixtures;

class ApiBandControllerTest extends WebTestCase
{

    public function setUp() {
        self::bootKernel();
        
        $manager = self::$container->get('doctrine.orm.entity_manager');

        $purger = new ORMPurger($manager);
        
        $purger->purge();
        $manager->getConnection()->exec("ALTER TABLE band AUTO_INCREMENT = 1;");
        $fixtures = new AppFixtures();
        $fixtures->load($manager);
    }

    public function testGetAll()
    {
        
        $client = static::createClient();
        $client->request('GET', '/api/band');

        $this->assertSame(200, $client->getResponse()->getStatusCode());
        
        $data = json_decode($client->getResponse()->getContent(), true);
        $this->assertCount(5, $data);
        $this->assertSame("Queen 0", $data[0]['name']);
    }

    public function testAddSuccess() {
        $client = static::createClient();

        $client->request('POST', '/api/band', [], [], [], json_encode([
            "name" => "test",
            "start" => "2019-04-29",
            "end" => "2019-04-30",
            "country" => "France"
        ]));

        $this->assertSame(201, $client->getResponse()->getStatusCode());
        $repo = self::$container->get('App\Repository\BandRepository');
        $this->assertSame(6, $repo->count([]));
    }

}
